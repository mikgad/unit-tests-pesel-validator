package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"83051601352", true},
                {"44051401358", false},
                {"8305160132", false},
                {"32840344822", true},
                {"00021027929", true},
                {"00301752851", true},
                {"83053401352", false},
                {"8305340135a", false},
                {"830534013522", false},
                {"20640744715", true},
                {"19491671006", true},
                {"-2", false},
                {"99022999369", false},
                {"00222997768", true},
        };
    }


    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid) {
        PeselValidator validator = new PeselValidator();
        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}