package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;

public class PeselTests {

    @DataProvider
    public Object[][] testGetDigitDataProvider() {
        return new Object[][]{

                {"83051601352", -1, -1},
                {"83051601352", 0, 8},
                {"83051601352", 11, -1},
                {"83051601352", 10, 2},
        };
    }

    @Test(dataProvider = "testGetDigitDataProvider")
    public void testGetDigit(String toParse, int position, int expResult) throws ParseException {
        Pesel getDigitTester = PeselParser.Parse(toParse);
        int actIndex = getDigitTester.getDigit(position);
        Assert.assertEquals(actIndex, expResult);
    }
}
