package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.time.LocalDate;

public class PeselExtractorTests {

    @DataProvider
    public Object[][] peselExtractorSexTestDataProvider() {
        return new Object[][]{
                {"83051601352", "Male"},
                {"00021027929", "Female"},
        };
    }

    @DataProvider
    public Object[][] peselExtractorBirthDayTestDataProvider() {
        return new Object[][]{
                {"83051601352", "1983-05-16"},
                {"00021027929", "1900-02-10"},
        };
    }


    @Test(dataProvider = "peselExtractorSexTestDataProvider")
    public void testGetSex(String peselData, String expectedSex) throws ParseException {
        Pesel pesel = PeselParser.Parse(peselData);
        PeselExtractor peselExtractor = new PeselExtractor(pesel);

        Assert.assertEquals(peselExtractor.GetSex(), expectedSex);
    }

    @Test(dataProvider = "peselExtractorBirthDayTestDataProvider")
    public void testGetData(String peselData, String expectedBirthDate) throws ParseException {

        Pesel pesel = PeselParser.Parse(peselData);
        PeselExtractor peselExtractor = new PeselExtractor(pesel);

        Assert.assertEquals(peselExtractor.GetBirthDate(), LocalDate.parse(expectedBirthDate));
    }

}