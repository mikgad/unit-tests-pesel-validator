package agh.qa;

import java.text.ParseException;

public class PeselValidator {
    private Pesel pesel;

    public boolean validate(String peselNumber) {
        if (!parse(peselNumber)) {
            System.out.println("PESEL " + peselNumber + " has invalid format");
            return false;
        }

        if (!isBirthDateValid(pesel)) {
            System.out.println("PESEL " + peselNumber + " has invalid birth date");
            return false;
        }

        if (!isCheckSumValid(pesel)) {
            System.out.println("PESEL " + peselNumber + " has invalid check sum");
            return false;
        }

        return true;
    }

    private boolean parse(String peselNumber) {
        try {
            pesel = PeselParser.Parse(peselNumber);
            return true;
        } catch (ParseException e) {
            System.out.println("PESEL " + peselNumber + " has invalid format");
            return false;
        }
    }

    /*  YEAR should be between 1800 and 2299 inclusive
     *   MONTH should be between 1 and 12 inclusive
     *   DAY should be between 1 and 28 / 29 / 30 / 31 inclusive (depends on month)
     * */
    private boolean isBirthDateValid(Pesel pesel) {
        int year = pesel.getBirthYear();
        int month = pesel.getBirthMonth();
        int day = pesel.getBirthDay();

        return isYearValid(year) && isMonthValid(month) && isDayValid(year, month, day);
    }

    private boolean isCheckSumValid(Pesel pesel) {
        return calculateCheckSum(pesel) == pesel.getCheckSum();
    }

    private boolean isYearValid(int year) {
        return year > 1799 && year < 2300;
    }

    private boolean isMonthValid(int month) {
        return month > 0 && month < 13;
    }

    private boolean isDayValid(int year, int month, int day) {
        if ((day > 0 && day < 32) && isMonth31(month)) {
            return true;
        } else if ((day > 0 && day < 31) && isMonth30(month)) {
            return true;
        } else return (day > 0 && day < 30 && isYearLeap(year)) ||
                (day > 0 && day < 29 && !isYearLeap(year));
    }

    private boolean isMonth31(int month) {
        return month == 1 || month == 3 || month == 5 ||
                month == 7 || month == 8 || month == 10 ||
                month == 12;
    }

    private boolean isMonth30(int month) {
        return month == 4 || month == 6 || month == 9 || month == 11;
    }

    private boolean isYearLeap(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }

    private int calculateCheckSum(Pesel pesel) {

        int checkSum =
                9 * pesel.getDigit(0) +
                        7 * pesel.getDigit(1) +
                        3 * pesel.getDigit(2) +
                        1 * pesel.getDigit(3) +
                        9 * pesel.getDigit(4) +
                        7 * pesel.getDigit(5) +
                        3 * pesel.getDigit(6) +
                        1 * pesel.getDigit(7) +
                        9 * pesel.getDigit(8) +
                        7 * pesel.getDigit(9);
        checkSum %= 10;


        return checkSum;
    }
}
